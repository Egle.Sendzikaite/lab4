package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    
    private int numberRows;
    private int numberCols;
    private CellState[][] cell;
    
    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.numberRows = rows;
        this.numberCols = columns;

        cell = new CellState[rows][columns];
        
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return numberRows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return numberCols;
    }

    private void rightIndexValue(int row, int column) {
        if (row >= numberRows || row < 0 || column >= numberCols || column < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        rightIndexValue(row, column);
        cell[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        rightIndexValue(row, column);
        return cell[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid newGrid = new CellGrid(numberRows, numberCols, null);
        for (int r = 0; r < numberRows; r++) {
            for (int c = 0; c < numberCols; c++) {
                newGrid.set(r, c, get(r, c));
            }
        }
        return newGrid;
    }
    
}
