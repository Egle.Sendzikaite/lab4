package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

	IGrid currentGeneration;


	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int row=0; row < numberOfRows(); row++) {
			for (int col=0; col < numberOfColumns(); col ++) {
				currentGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		/**
		 * En levende celle blir døende
         * En døende celle blir død
         * En død celle med akkurat 2 levende naboer blir levende
         * En død celle forblir død ellers
		*/
		CellState state = currentGeneration.get(row, col);
        if (state == CellState.ALIVE) {return CellState.DYING; }
        else if (state == CellState.DYING) {return CellState.DEAD; }
        else {
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {return CellState.ALIVE; }
            else {return CellState.DEAD; }
            }
        }
	
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int neighbors = 0;
		if (currentGeneration.get(row, col).equals(state)) {neighbors--;}
		for (int r = -1; r < 2; r++) {
			if (row + r < numberOfRows() && row + r >= 0) {for (int c = -1; c < 2; c++) {
				if (col + c < numberOfColumns() && col + c >= 0) {if (currentGeneration.get(row + r, col + c).equals(state)) 
					{neighbors++;}}
				}
			}
		}
		return neighbors;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
    
